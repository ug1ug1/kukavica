package com.iwec.endpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.iwec.books.model.BookDetailsRequest;
import com.iwec.books.model.BookDetailsResponse;
import com.iwec.books.model.InsertBookRequest;
import com.iwec.books.model.InsertBookResponse;
import com.iwec.repository.BooksRepository;

@Endpoint
public class BookEndPoint {
	private static final String NAMESPACE_URI = "model.books.iwec.com";

	@Autowired
	private BooksRepository bookRepository;

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "BookDetailsRequest")
	@ResponsePayload
	public BookDetailsResponse getStudent(
			@RequestPayload BookDetailsRequest request) {
		BookDetailsResponse response = new BookDetailsResponse();
		response.setBook(bookRepository.findBook(request.getId()));

		return response;
	}
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "InsertBookRequest")
	@ResponsePayload
	public InsertBookResponse getStudent(
			@RequestPayload InsertBookRequest request) {
		InsertBookResponse response = new InsertBookResponse();
		response.setBook(bookRepository.insertBook(request.getId(), request.getTitle(),request.getSubtitle(),request.getIsbn()));

		return response;
}
}