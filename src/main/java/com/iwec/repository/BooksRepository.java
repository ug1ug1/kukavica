package com.iwec.repository;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.iwec.books.model.Book;

@Component
public class BooksRepository {
	private static final Map<Integer, Book> books = new TreeMap<>();

	@PostConstruct
	public void initData() {

		Book book = new Book();
		book.setId(1);
		book.setTitle("Robinson Kruso");
		book.setSubtitle("Dvojkata");
		book.setIsbn("1234567890123");
		books.put(book.getId(), book);

	    book = new Book();
		book.setId(2);
		book.setTitle("Robsinson Kruso");
		book.setSubtitle("Trojkata");
		book.setIsbn("1233567890123");
		books.put(book.getId(), book);

	}

	public Book findBook(Integer id) {
		Assert.notNull(id, "The Book's ID must not be null.");
		return books.get(id);
	}
	
	public Book insertBook(Integer id, String title, String subtitle, String isbn) {
		Book book = new Book();
		book.setId(id);
		book.setTitle(title);
		book.setSubtitle(subtitle);
		book.setIsbn(isbn);
		
		return books.put(id , book);
		
	}

}
	